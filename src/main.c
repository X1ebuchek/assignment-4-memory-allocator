#include "mem.h"
#include "mem_internals.h"
#include <stdint.h>
#include <stdio.h>


int common_successful_allocate_test(size_t size){
    printf("Test1\n");
    heap_init(3000);
    int64_t* test = _malloc(size);
    if (test){
        printf("Success\n");
        _free(test);
        return true;
    }
    printf("Fail\n");
    _free(test);
    return false;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

int free_one_block(size_t size){
    printf("\nTest2\n");
    heap_init(size);

    int64_t* test_1 = _malloc(3000);
    int64_t* test_2 = _malloc(4000);
    struct block_header* block_1 = block_get_header(test_1);
    if (!block_1->is_free) printf("allocate block 1\n");
    struct block_header* block_2 = block_get_header(test_2);
    if (!block_2->is_free) printf("allocate block 2\n");

    _free(test_1);
    if (block_1->is_free && !block_2->is_free){
        printf("Success\n");
        _free(test_2);
        return true;
    }
    printf("Fail\n");
    return false;
}

int free_two_blocks(size_t size){
    printf("\nTest3\n");
    heap_init(size);

    int64_t* test_1 = _malloc(3000);
    int64_t* test_2 = _malloc(4000);
    int64_t* test_3 = _malloc(5000);
    struct block_header* block_1 = block_get_header(test_1);
    if (!block_1->is_free){
        printf("allocate block 1\n");
    }

    struct block_header* block_2 = block_get_header(test_2);
    if (!block_2->is_free){
        printf("allocate block 2\n");
    }

    struct block_header* block_3 = block_get_header(test_3);
    if (!block_3->is_free){
        printf("allocate block 3\n");
    }
    _free(test_1);
    _free(test_2);
    if (block_1->is_free && block_2->is_free && !block_3->is_free){
        printf("Success\n");
        _free(test_3);
        return true;
    }
    printf("Fail\n");
    return false;
}

int new_memory_expands_old(size_t size){
    printf("\nTest4\n");
    heap_init(size);

    int64_t* test_1  = _malloc(3000);
    struct block_header* block = block_get_header(test_1);
    debug_heap(stdout,block);
    int64_t* test_2 = _malloc(4000);
    int64_t* test_3 = _malloc(5000);
    debug_heap(stdout,block);

    if (test_2) printf("Success\n");
    else printf("Fail\n");

    _free(test_1);
    _free(test_2);
    _free(test_3);

    return 0;
}

int new_memory_is_allocated_elsewhere(size_t size){
    printf("\nTest5\n");
    heap_init(size);

    int64_t* test_1 = _malloc(3000);
    struct block_header *block_1 = block_get_header(test_1);
    debug_heap(stdout, block_1);

    struct block_header* next_block = block_1->next;
    void* offset = next_block->contents + next_block->capacity.bytes;
    void* mp = mmap(offset, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20, -1, 0);

    if (!mp) printf("Fail\n");

    int64_t* test_2 = _malloc(4000);

    debug_heap(stdout, block_1);
    struct block_header* block_2 = block_get_header(test_2);

    if (!block_1->is_free && !block_2->is_free) {
        _free(test_1);
        _free(test_2);
        debug_heap(stdout, block_1);
    }
    printf("Success\n");

    return 0;
}

int main(){
    common_successful_allocate_test(3);
    free_one_block(3);
    free_two_blocks(3);
    new_memory_expands_old(3);
    new_memory_is_allocated_elsewhere(3);
}
